import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface Product {
  id: number;
  name: string;
  price: number;
  amount: number;
}

@Injectable({
  providedIn: 'root'
})
export class CartService {

  data: Product[] = [
    { id: 0, name: 'Pizza Salami', price: 8.99, amount: 1 },
    { id: 1, name: 'Pizza Classic', price: 5.49, amount: 1 },
    { id: 2, name: 'Sliced Bread', price: 4.99, amount: 1 },
    { id: 3, name: 'Salad', price: 6.99, amount: 1 }
  ];

  private cart = [];
  private cartItemCount = new BehaviorSubject(0);

  constructor() { }

  // Getting products data
  getProducts() {
    return this.data;
  }


  // Getting cart information
  getCart() {
    return this.cart;
  }


  // Getting cart item count
  getCartItemCount() {
    return this.cartItemCount;
  }


  // Adding product to cart
  addProduct(product) {
    let added = false;

    // Increase product amount count if product is already existing in cart
    for (const p of this.cart) {
      if (p.id === product.id) {
        p.amount += 1;
        added = true;
        break;
      }
    }

    // if added product isn`t already existing in cart push to cart
    if (!added) {
      this.cart.push(product);
    }

    // Update cart item count by adding 1
    this.cartItemCount.next(this.cartItemCount.value + 1);
  }


  // Decrease product amount from cart
  decreaseProduct(product) {
    for (let [index, p] of this.cart.entries()) {
      if (p.id === product.id) {
        p.amount -= 1;

        // if the amount count is 0, remove from cart
        if (p.amount === 0) {
          this.cart.slice(index, 1);
        }
      }
    }

    // Update cart item count by removing 1
    this.cartItemCount.next(this.cartItemCount.value -1);
  }


  // Remove product from cart
  removeProduct(product) {
    for (let [index, p] of this.cart.entries()) {
      if (p.id === product.id) {
        this.cartItemCount.next(this.cartItemCount.value - p.amount);
        this.cart.splice(index, 1);
      }
    }
  }

}
